use std::collections::{HashMap};
use std::io;
use std::io::{Cursor, Read};
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4, TcpListener, UdpSocket};
use std::sync::{Arc};
use std::time::{Duration, Instant};

use mio::*;
use mio::{Poll, Events};
use net2::{UdpBuilder};

use bytes::{Bytes};
use threading::{Message, ThreadGlobal, ThreadID, send_message};

// Represents the network connections one process uses
// to meet up with other network members.
pub struct NetworkListener {
    // A human-understandable identifier for this process.
    identifier: String,
    // Maps these identifiers to the addresses via which to communicate.
    known_hosts: HashMap<String, SocketAddr>,
    // Multicasts packets to announce this process exists, and receives them.
    greeter: UdpSocket,
    // The socket that accepts the messages from other processes.
    communicator: TcpListener,
}

// The address we use to say hello.
const MULTICAST_ADDRESS: &'static str = "237.59.133.44";
// The above address, with port.
const MULTICAST_SOCKET: &'static str = "237.59.133.44:4547";
// The distance along the network we advertise ourselves.
const MULTICAST_TTL: u32 = 1;

// Create a network listener.
// This listener hasn't started listening yet;
// for that you can use start_listening.
pub fn make_listener(identifier: String) -> io::Result<NetworkListener> {
    let address: Ipv4Addr = MULTICAST_ADDRESS.parse().unwrap();
    let default_address: Ipv4Addr = "0.0.0.0".parse().unwrap();
    let port = 4547;

    // Create the greeting socket.
    let greeter = UdpBuilder::new_v4()?
        .reuse_address(true)?
        .bind(SocketAddrV4::new(default_address, port))?;
    // Make sure other processes receive messages.
    greeter.set_broadcast(true)?;
    greeter.set_multicast_loop_v4(true)?;
    // Make sure we can receive messages.
    greeter.join_multicast_v4(&address, &default_address)?;
    greeter.set_multicast_ttl_v4(MULTICAST_TTL)?;
    // When the channel has been quiet for a while, send greetings.
    greeter.set_read_timeout(Some(Duration::from_secs(1)))
        .expect("setting timeout");
    // Note that we don't use .connect() as that will filter out our intended messages.

    // Open an arbitrary address/port for communicating.
    let communicator = TcpListener::bind("0.0.0.0:0")?;

    info!("greeter: {:?}, communicator: {:?}", greeter.local_addr(), communicator.local_addr());

    Ok(NetworkListener {
        identifier: identifier,
        known_hosts: HashMap::new(),
        greeter: greeter,
        communicator: communicator,
    })
}

// Listen for connections and messages.
// This is a blocking operation, so it would be a good idea
// to spawn a thread that calls start_listening.
pub fn start_listening(mut listener: NetworkListener, threads: Arc<ThreadGlobal>) {
    const GREETER: Token = Token(0);
    const COMMUNICATOR: Token = Token(1);
    // Increase the token number each time we get a new client.
    // TODO: we must be able to do this better
    let mut last_token = 1;

    let greeter = net::UdpSocket::from_socket(listener.greeter)
        .expect("could not create greeter socket");
    let addr = listener.communicator.local_addr()
        .expect("could not get local address");
    let communicator = net::TcpListener::from_listener(listener.communicator, &addr)
        .expect("could not create communicator listener");

    let poll = Poll::new().unwrap();
    poll.register(&greeter, GREETER, Ready::readable(),
        PollOpt::edge()).unwrap();
    poll.register(&communicator, COMMUNICATOR, Ready::readable(),
        PollOpt::edge()).unwrap();

    // Create storage for events
    let mut events = Events::with_capacity(1024);
    // And for streams to other hosts
    let mut streams = HashMap::new();

    // The last time we announced ourselves.
    send_hello(&greeter, &addr, &listener.identifier);
    let mut last_announce = Instant::now();

    loop {
        poll.poll(&mut events, Some(Duration::from_secs(1)))
            .expect("could not poll for events");

        for event in events.iter() {
            match event.token() {
                GREETER => {
                    if let Some((identifier, address)) = recv_hello(&greeter) {
                        info!("discovered {} at {}", identifier, address);
                        listener.known_hosts.insert(identifier, address);
                    }
                }
                COMMUNICATOR => {
                    if let Ok((stream, peer)) = communicator.accept() {
                        last_token += 1;
                        poll.register(&stream, Token(last_token), Ready::readable(),
                                PollOpt::edge()).unwrap();
                        streams.insert(last_token, (stream, peer));
                    } else {
                        warn!("could not accept connection");
                    }
                }
                Token(n) => {
                    let (ref stream, ref peer) = streams[&n];
                    if let Some((thread_id, message)) = recv_message(stream, peer) {
                        send_message(&threads, thread_id, message);
                    } else {
                        warn!("got bad message from {:?}", peer);
                    }
                }
            }
        }

        if last_announce.elapsed() > Duration::from_secs(10) {
            send_hello(&greeter, &addr, &listener.identifier);
            last_announce = Instant::now();
        }
    }
}

// Notify on the socket that this process exists.
fn send_hello(socket: &net::UdpSocket, address: &SocketAddr, identifier: &str) {
    let mut message: Vec<u8> = Vec::new();
    message.push(0x01);
    message.append(&mut address.port().to_bytes());
    message.append(&mut String::from(identifier).to_bytes());
    info!("sending {:?}", message);
    socket.send_to(&message, &MULTICAST_SOCKET.parse().unwrap())
        .expect("multicasting hello");
}
// Get a hello message from the socket and return it.
fn recv_hello(socket: &net::UdpSocket) -> Option<(String, SocketAddr)> {
    // Receive the message
    let mut buf = [0; 256];
    let received = socket.recv_from(&mut buf);
    if let Err(e) = received {
        warn!("recv function failed: {:?}", e);
        return None;
    }
    let (length, mut sender) = received.unwrap();
    let message = &buf[..length];
    info!("received {:?} from {:?}", message, &sender);

    // Parse the message
    let mut cursor = Cursor::new(message);
    let mut header = [0; 1];
    if let Err(e) = cursor.read_exact(&mut header) {
        warn!("recv function failed: {:?}", e);
        return None;
    }

    match header[0] {
        0x01 => {
            // Respond with the sender but at their communicator port.
            u16::from_bytes(&mut cursor).ok()
                .and_then(|port| String::from_bytes(&mut cursor).ok()
                .map(|identifier| {
                    sender.set_port(port);
                    (identifier, sender)
            }))
        }
        n => {
            warn!("ignoring unknown message format {}", n);
            None
        }
    }
}

// Read a message from the socket to a thread.
fn recv_message(mut socket: &net::TcpStream, peer: &SocketAddr) -> Option<(ThreadID, Message)> {
    // TODO: some form of encryption
    // TODO: handle filling of the buffer with stuff to read,
    // TODO: handle TCP like a stream of bytes, not a stream of packets
    let mut buf = [0; 256];
    let received = socket.read(&mut buf);
    if let Err(e) = received {
        warn!("recv message failed: {:?}", e);
        return None;
    }
    let length = received.unwrap();
    let message = &buf[..length];
    info!("received {:?} from {:?}", message, peer);

    // Parse the message
    let mut cursor = Cursor::new(message);
    let mut header = [0; 1];
    if let Err(e) = cursor.read_exact(&mut header) {
        warn!("recv function failed: {:?}", e);
        return None;
    }
    match header[0] {
        0x02 => {
            let mut bytes = Vec::new();
            u64::from_bytes(&mut cursor).ok()
                .and_then(|thread_id| cursor.read_to_end(&mut bytes).ok()
                .map(|_| (thread_id, bytes)
            ))
        }
        n => {
            warn!("ignoring unknown message format {}", n);
            None
        }
    }
}
