use as_num::{AsNum};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use std::error::{Error};
use std::fmt;
use std::marker::{Sized};
use std::io;
use std::io::{Cursor, ErrorKind, Read};

#[derive(Debug)]
pub enum SerializeError {
    FormatError,
}

impl fmt::Display for SerializeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            SerializeError::FormatError => write!(f, "invalid serialization format"),
        }
    }
}

impl Error for SerializeError {
    fn description(&self) -> &str {
        match *self {
            SerializeError::FormatError => "invalid serialization format",
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            SerializeError::FormatError => None,
        }
    }
}

// Represents a number or similar that can be represented as bytes.
pub trait Bytes
    where Self: Sized,
{
    fn to_bytes(self) -> Vec<u8>;
    fn from_bytes(&mut Cursor<&[u8]>) -> io::Result<Self>;
}

impl Bytes for u16 {
    fn to_bytes(self) -> Vec<u8> {
        let mut rdr = Cursor::new(Vec::new());
        rdr.write_u16::<BigEndian>(self).expect("could not write u16 to vec");
        rdr.into_inner()
    }
    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<u16> {
        buf.read_u16::<BigEndian>()
    }
}
impl Bytes for u64 {
    fn to_bytes(self) -> Vec<u8> {
        let mut rdr = Cursor::new(Vec::new());
        rdr.write_u64::<BigEndian>(self).expect("could not write u64 to vec");
        rdr.into_inner()
    }
    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<u64> {
        buf.read_u64::<BigEndian>()
    }
}
impl Bytes for usize {
    fn to_bytes(self) -> Vec<u8> {
        let mut rdr = Cursor::new(Vec::new());
        rdr.write_u64::<BigEndian>(self.as_num()).expect("could not write usize to vec");
        rdr.into_inner()
    }
    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<usize> {
        buf.read_u64::<BigEndian>().map(|n| n.as_num())
    }
}

impl Bytes for String {
    fn to_bytes(self) -> Vec<u8> {
        let mut result = self.len().to_bytes();
        result.append(&mut self.into_bytes());
        result
    }

    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<String> {
        let length = usize::from_bytes(buf)?;
        let mut bytes = vec![0; length];
        buf.read_exact(&mut bytes)?;
        String::from_utf8(bytes).map_err(|err| io::Error::new(ErrorKind::Other, err))
    }
}
