use std::io;

// Type of De Bruijn indices.
pub type Index = u64;

// The following functions are builtins, implemented by the system.
#[derive(Clone, Debug, PartialEq)]
pub enum SysCall {
    // Stop execution for this thread.
    // Parameter 1 is the exit status (which is sent to the monitors).
    Exit(Box<SwailData>),
    // Write bytes to stdout.
    // Parameter 1 is the bytes to write.
    Write(Box<SwailData>),
    // Read bytes from stdin.
    // Result is a byte array.
    Read(),
    // Convert the expression to bytes.
    Serialize(Box<SwailData>),
    // Convert the bytes to an expression.
    Unserialize(Box<SwailData>),
    // Create a new thread.
    // Parameter 1 is the function to execute.
    Fork(Box<SwailData>),
    // Send a message to the given thread.
    // Parameter 1 is the thread id,
    // parameter 2 is the message to send.
    Send(Box<SwailData>, Box<SwailData>),
    // Pause and receive the first unreceived message.
    Receive(),
    // Get the ID of the current thread.
    ThreadID(),
    // Generate a new keypair.
    NewKey(),
    // Extract the public key from a private key.
    ToPublic(Box<SwailData>),
    // Encrypt and sign bytes with public and private key.
    EncryptSigned(Box<SwailData>, Box<SwailData>, Box<SwailData>),
    // Verify and decrypt bytes with public and private key.
    VerifyDecrypted(Box<SwailData>, Box<SwailData>, Box<SwailData>),
}

// The types of fully evaluated values.
#[derive(Clone, Debug, PartialEq)]
pub enum SwailType {
    Lambda,
    Bytes,
}

// Represents the values that may appear in Swail memory.
// (Of course, this is not very optimized, but optimizing is for the weak.)
#[derive(Clone, Debug, PartialEq)]
pub enum SwailData {
    // When applied, fills in the applied vale for the outermost de Bruijn value.
    Lambda(Box<SwailData>),
    // A variable with a De Bruijn index.
    Variable(Index),
    // Apply a function to an argument.
    Apply(Box<SwailData>, Box<SwailData>),
    // An arbitrary set of data, which you can't do much with,
    // except when you have the required library functions.
    // This is very useful for FFI purposes (e.g. storing pointers).
    Bytes(Box<[u8]>),
    // Call a builtin function.
    // The argument must evaluate to a function,
    // which will be called with the call's return data;
    // think continuation-passing style.
    Call(SysCall, Box<SwailData>),
}

#[derive(Debug, PartialEq)]
pub enum SwailError {
    // You tried to evaluate a variable which wasn't bound.
    NameError(),
    // This operation doesn't make any sense.
    TypeError(SwailType),
    // Thread execution stopped, with the following status.
    StopExecution(SwailData),
    // Error during reading/writing of file.
    FileError(io::ErrorKind),
    // Error during serializing/unserializing values.
    BufferError(io::ErrorKind),
}

// Convenience functions for defining stuff without wrapping.
pub fn make_lambda(body: SwailData) -> SwailData {
    SwailData::Lambda(Box::new(body))
}
pub fn make_variable(i: Index) -> SwailData {
    SwailData::Variable(i)
}
pub fn make_apply(func: SwailData, arg: SwailData) -> SwailData {
    SwailData::Apply(Box::new(func), Box::new(arg))
}
pub fn make_bytes_vec(data: Vec<u8>) -> SwailData {
    SwailData::Bytes(data.into_boxed_slice())
}
pub fn make_bytes_buf(data: &[u8], size: usize) -> SwailData {
    let mut copy = Vec::with_capacity(size);
    copy.extend_from_slice(&data[0..size]);
    SwailData::Bytes(copy.into_boxed_slice())
}
pub fn make_call(call: SysCall, continuation: SwailData) -> SwailData {
    SwailData::Call(call, Box::new(continuation))
}
