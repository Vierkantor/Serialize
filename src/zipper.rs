use std::convert::{AsRef};
use std::marker::{Sized};

use data::{SwailData, SysCall};

// Represents whether the zipper moved somewhere,
// or stayed in the same place.
pub enum Movement<T> {
    Moved(T),
    Stayed(T),
}
use self::Movement::{Moved, Stayed};

impl<T> Movement<T> {
    // Expect that there was movement.
    pub fn expect(self, err: &str) -> T {
        match self {
            Moved(t) => t,
            Stayed(_) => panic!("{}", err),
        }
    }
}

// Represents data with a point in focus,
// whose point can be moved, and its value replaced,
// very easily.
pub trait Zipper
    where Self: Sized
{
    type Zipped;

    fn zip(data: Self::Zipped) -> Self;
    fn unzip(data: Self) -> Self::Zipped;

    fn up(zipper: Self) -> Movement<Self>;
    fn down(zipper: Self) -> Movement<Self>;
    fn left(zipper: Self) -> Movement<Self>;
    fn right(zipper: Self) -> Movement<Self>;
}

// Describes the point of the call that we're currently at.
#[derive(Clone, Debug, PartialEq)]
pub enum CallContext {
    ZExit(Box<DataContext>),
    ZWrite(Box<DataContext>),
    ZSerialize(Box<DataContext>),
    ZUnserialize(Box<DataContext>),
    ZFork(Box<DataContext>),
    ZSendL(Box<DataContext>, Box<SwailData>),
    ZSendR(Box<SwailData>, Box<DataContext>),
    ZToPublic(Box<DataContext>),
    ZEncryptSignedL(Box<DataContext>, Box<SwailData>, Box<SwailData>),
    ZEncryptSignedM(Box<SwailData>, Box<DataContext>, Box<SwailData>),
    ZEncryptSignedR(Box<SwailData>, Box<SwailData>, Box<DataContext>),
    ZVerifyDecryptedL(Box<DataContext>, Box<SwailData>, Box<SwailData>),
    ZVerifyDecryptedM(Box<SwailData>, Box<DataContext>, Box<SwailData>),
    ZVerifyDecryptedR(Box<SwailData>, Box<SwailData>, Box<DataContext>),
}

// Describes the point of the evaluation that we're currently at.
#[derive(Clone, Debug, PartialEq)]
pub enum DataContext {
    // We haven't gone into the zipper.
    ZHere(),
    // We're pointing to a lambda term, with some context outside and data inside.
    ZLambda(Box<DataContext>),
    // Application with one of the parts a hole.
    ZApplyL(Box<DataContext>, Box<SwailData>),
    ZApplyR(Box<SwailData>, Box<DataContext>),
    // System call with one of the arguments a hole.
    // (Note that the continuation is the leftmost argument,
    // even though it's represented to the right.)
    ZCallL(SysCall, Box<DataContext>),
    ZCallR(CallContext, Box<SwailData>),
}

#[derive(Clone, Debug, PartialEq)]
pub struct DataZipper {
    pub higher: Box<DataContext>,
    pub lower: Box<SwailData>,
}

// Make a zipper by boxing the data.
pub fn data_zipper(ctx: Box<DataContext>, data: SwailData) -> DataZipper {
    DataZipper{higher: ctx, lower: Box::new(data)}
}

impl Zipper for DataZipper {
    type Zipped = Box<SwailData>;

    fn up(zipper: DataZipper) -> Movement<DataZipper> {
        let data = zipper.lower;
        // this is a workaround to issue #16223, I think...
        match (*zipper.higher,) {
            (ctx@DataContext::ZHere(),)
                => Stayed(DataZipper{higher: box ctx, lower: data}),
            (DataContext::ZLambda(ctx),)
                => Moved(data_zipper(ctx, SwailData::Lambda(data))),
            (DataContext::ZApplyL(ctx, rhs),)
                => Moved(data_zipper(ctx, SwailData::Apply(data, rhs))),
            (DataContext::ZApplyR(lhs, ctx),)
                => Moved(data_zipper(ctx, SwailData::Apply(lhs, data))),
            (DataContext::ZCallR(call_ctx, cont),) => {
                let (ctx, call) = match call_ctx {
                    CallContext::ZExit(ctx) => (ctx, SysCall::Exit(data)),
                    CallContext::ZWrite(ctx) => (ctx, SysCall::Write(data)),
                    CallContext::ZSerialize(ctx) => (ctx, SysCall::Serialize(data)),
                    CallContext::ZUnserialize(ctx) => (ctx, SysCall::Unserialize(data)),
                    CallContext::ZFork(ctx) => (ctx, SysCall::Fork(data)),
                    CallContext::ZSendL(ctx, msg) => (ctx, SysCall::Send(data, msg)),
                    CallContext::ZSendR(tid, ctx) => (ctx, SysCall::Send(tid, data)),
                    CallContext::ZToPublic(ctx) => (ctx, SysCall::ToPublic(data)),
                    CallContext::ZEncryptSignedL(ctx, their, our)
                        => (ctx, SysCall::EncryptSigned(data, their, our)),
                    CallContext::ZEncryptSignedM(bytes, ctx, our)
                        => (ctx, SysCall::EncryptSigned(bytes, data, our)),
                    CallContext::ZEncryptSignedR(bytes, their, ctx)
                        => (ctx, SysCall::EncryptSigned(bytes, their, data)),
                    CallContext::ZVerifyDecryptedL(ctx, their, our)
                        => (ctx, SysCall::VerifyDecrypted(data, their, our)),
                    CallContext::ZVerifyDecryptedM(bytes, ctx, our)
                        => (ctx, SysCall::VerifyDecrypted(bytes, data, our)),
                    CallContext::ZVerifyDecryptedR(bytes, their, ctx)
                        => (ctx, SysCall::VerifyDecrypted(bytes, their, data)),
                };
                Moved(data_zipper(ctx, SwailData::Call(call, cont)))
            }
            (DataContext::ZCallL(call, ctx),)
                => Moved(data_zipper(ctx, SwailData::Call(call, data))),
        }
    }

    fn down(zipper: DataZipper) -> Movement<DataZipper> {
        let ctx = zipper.higher;
        match (*zipper.lower,) {
            (SwailData::Lambda(body),)
                => Moved(DataZipper{
                    higher: Box::new(DataContext::ZLambda(ctx))
                    , lower: body
                }),
            (SwailData::Apply(lhs, rhs),)
                => Moved(DataZipper{
                    higher: Box::new(DataContext::ZApplyL(ctx, rhs))
                    , lower: lhs
                }),
            (SwailData::Call(call, cont),)
                => Moved(DataZipper{
                    higher: Box::new(DataContext::ZCallL(call, ctx))
                    , lower: cont
                }),
            (data,) => Stayed(DataZipper{higher: ctx, lower: box data}),
        }
    }

    fn left(zipper: DataZipper) -> Movement<DataZipper> {
        match (*zipper.higher,) {
            (DataContext::ZApplyR(lhs, ctx),)
                => Moved(DataZipper{
                    higher: Box::new(DataContext::ZApplyL(ctx, zipper.lower)),
                    lower: lhs,
                }),
            (DataContext::ZCallR(call_ctx, cont),) => {
                let (ctx, syscall) = match call_ctx {
                    CallContext::ZExit(ctx)
                        => (ctx, SysCall::Exit(zipper.lower)),
                    CallContext::ZWrite(ctx)
                        => (ctx, SysCall::Write(zipper.lower)),
                    CallContext::ZSerialize(ctx)
                        => (ctx, SysCall::Serialize(zipper.lower)),
                    CallContext::ZUnserialize(ctx)
                        => (ctx, SysCall::Unserialize(zipper.lower)),
                    CallContext::ZFork(ctx)
                        => (ctx, SysCall::Fork(zipper.lower)),
                    CallContext::ZSendL(ctx, msg)
                        => (ctx, SysCall::Send(zipper.lower, msg)),
                    CallContext::ZSendR(tid, ctx) => {
                        let call_ctx = CallContext::ZSendL(ctx, zipper.lower);
                        return Moved(DataZipper{
                            higher: box DataContext::ZCallR(call_ctx, cont),
                            lower: tid,
                        });
                    },
                    CallContext::ZToPublic(ctx)
                        => (ctx, SysCall::ToPublic(zipper.lower)),
                    CallContext::ZEncryptSignedL(ctx, their, our)
                        => (ctx, SysCall::EncryptSigned(zipper.lower, their, our)),
                    CallContext::ZEncryptSignedM(bytes, ctx, our) => {
                        let call_ctx = CallContext::ZEncryptSignedL(ctx, zipper.lower, our);
                        return Moved(DataZipper{
                            higher: box DataContext::ZCallR(call_ctx, cont),
                            lower: bytes,
                        });
                    },
                    CallContext::ZEncryptSignedR(bytes, their, ctx) => {
                        let call_ctx = CallContext::ZEncryptSignedM(bytes, ctx, zipper.lower);
                        return Moved(DataZipper{
                            higher: box DataContext::ZCallR(call_ctx, cont),
                            lower: their,
                        });
                    },
                    CallContext::ZVerifyDecryptedL(ctx, their, our)
                        => (ctx, SysCall::VerifyDecrypted(zipper.lower, their, our)),
                    CallContext::ZVerifyDecryptedM(bytes, ctx, our) => {
                        let call_ctx = CallContext::ZVerifyDecryptedL(ctx, zipper.lower, our);
                        return Moved(DataZipper{
                            higher: box DataContext::ZCallR(call_ctx, cont),
                            lower: bytes,
                        });
                    },
                    CallContext::ZVerifyDecryptedR(bytes, their, ctx) => {
                        let call_ctx = CallContext::ZVerifyDecryptedM(bytes, ctx, zipper.lower);
                        return Moved(DataZipper{
                            higher: box DataContext::ZCallR(call_ctx, cont),
                            lower: their,
                        });
                    },
                };
                Moved(DataZipper{
                    higher: Box::new(DataContext::ZCallL(syscall, ctx)),
                    lower: cont,
                })
            }
            (ctx,) => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
        }
    }

    fn right(zipper: DataZipper) -> Movement<DataZipper> {
        match (*zipper.higher,) {
            (ctx@DataContext::ZHere(),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (ctx@DataContext::ZLambda(_),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (ctx@DataContext::ZApplyR(_, _),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (DataContext::ZApplyL(ctx, rhs),)
                => Moved(DataZipper{
                    higher: Box::new(DataContext::ZApplyR(zipper.lower, ctx)),
                    lower: rhs,
                }),
            // TODO: can we do less special-casing?
            (ctx@DataContext::ZCallL(SysCall::Read(), _),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (ctx@DataContext::ZCallL(SysCall::Receive(), _),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (ctx@DataContext::ZCallL(SysCall::ThreadID(), _),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (ctx@DataContext::ZCallL(SysCall::NewKey(), _),)
                => Stayed(DataZipper{higher: box ctx, lower: zipper.lower}),
            (DataContext::ZCallL(call, ctx),) => {
                let (arg, call_ctx) = match call {
                    SysCall::Exit(a) => (a, CallContext::ZExit(ctx)),
                    SysCall::Read() => panic!("nowhere to go; this should be handled up higher."),
                    SysCall::Write(a) => (a, CallContext::ZWrite(ctx)),
                    SysCall::Serialize(a) => (a, CallContext::ZSerialize(ctx)),
                    SysCall::Unserialize(a) => (a, CallContext::ZUnserialize(ctx)),
                    SysCall::Fork(a) => (a, CallContext::ZFork(ctx)),
                    SysCall::Send(a, msg) => (a, CallContext::ZSendL(ctx, msg)),
                    SysCall::Receive() => panic!("nowhere to go; this should be handled up higher."),
                    SysCall::ThreadID() => panic!("nowhere to go; this should be handled up higher."),
                    SysCall::NewKey() => panic!("nowhere to go; this should be handled up higher."),
                    SysCall::ToPublic(a) => (a, CallContext::ZToPublic(ctx)),
                    SysCall::EncryptSigned(a, their, our)
                        => (a, CallContext::ZEncryptSignedL(ctx, their, our)),
                    SysCall::VerifyDecrypted(a, their, our)
                        => (a, CallContext::ZVerifyDecryptedL(ctx, their, our)),
                };
                Moved(DataZipper{
                    higher: Box::new(DataContext::ZCallR(call_ctx, zipper.lower)),
                    lower: arg,
                })
            }
            (DataContext::ZCallR(call, cont),) => {
                let maybe_call_ctx = match call {
                    CallContext::ZExit(_) => Err((call, zipper.lower)),
                    CallContext::ZWrite(_) => Err((call, zipper.lower)),
                    CallContext::ZSerialize(_) => Err((call, zipper.lower)),
                    CallContext::ZUnserialize(_) => Err((call, zipper.lower)),
                    CallContext::ZFork(_) => Err((call, zipper.lower)),
                    CallContext::ZSendL(ctx, msg) => {
                        Ok((CallContext::ZSendR(zipper.lower, ctx), msg))
                    }
                    CallContext::ZSendR(_, _) => Err((call, zipper.lower)),
                    CallContext::ZToPublic(_) => Err((call, zipper.lower)),
                    CallContext::ZEncryptSignedL(ctx, their, our) => {
                        Ok((CallContext::ZEncryptSignedM(zipper.lower, ctx, our), their))
                    }
                    CallContext::ZEncryptSignedM(bytes, ctx, our) => {
                        Ok((CallContext::ZEncryptSignedR(bytes, zipper.lower, ctx), our))
                    }
                    CallContext::ZEncryptSignedR(_, _, _) => Err((call, zipper.lower)),
                    CallContext::ZVerifyDecryptedL(ctx, their, our) => {
                        Ok((CallContext::ZVerifyDecryptedM(zipper.lower, ctx, our), their))
                    }
                    CallContext::ZVerifyDecryptedM(bytes, ctx, our) => {
                        Ok((CallContext::ZVerifyDecryptedR(bytes, zipper.lower, ctx), our))
                    }
                    CallContext::ZVerifyDecryptedR(_, _, _) => Err((call, zipper.lower)),
                };
                match maybe_call_ctx {
                    Ok((call_ctx, data))
                        => Moved(DataZipper{
                            higher: box DataContext::ZCallR(call_ctx, cont),
                            lower: data
                        }),
                    Err((call, lower))
                        => Stayed(DataZipper{
                            higher: box DataContext::ZCallR(call, cont),
                            lower: lower,
                        }),
                }
            }
        }
    }

    fn zip(data: Box<SwailData>) -> DataZipper {
        DataZipper{higher: Box::new(DataContext::ZHere()), lower: data}
    }

    fn unzip(zipper: DataZipper) -> Box<SwailData> {
        let mut result: DataZipper = zipper;
        loop {
            match Self::up(result) {
                Moved(next_pos) => {
                    result = next_pos;
                }
                Stayed(this_pos) => {
                    let DataZipper{higher: ctx, lower: data} = this_pos;
                    match *ctx.as_ref() {
                        DataContext::ZHere() => return data,
                        _ => panic!("unzip stopped before discarding context"),
                    }
                }
            }
        }
    }
}
