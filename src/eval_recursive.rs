use std::io;
use std::io::{Cursor, Read, Write};
use std::ops::{Deref};

use data::{Index,
    SwailData, SwailError, SwailType, SysCall,
    make_bytes_buf, make_bytes_vec,
};
use derived::{make_id};
use serialize::{serialize, unserialize};

// Substitute the variable with given abstraction depth in the body.
fn subs(body: &SwailData, val: &SwailData, depth: Index)
        -> Result<SwailData, SwailError> {
    info!(target: "subs", "body: {:?} val: {:?} depth: {:?}", body, val, depth);
    match *body {
        // We have to go deeper!
        SwailData::Lambda(ref body) => subs(body.deref(), val, depth + 1)
            .map(|sub_body| SwailData::Lambda(Box::new(sub_body))),
        // Substitute the right values.
        SwailData::Variable(var) => {if depth == var {
                Ok(val.clone())
            } else {
                Ok(body.clone())
            }
        }
        // Bytes can be passed identically.
        SwailData::Bytes(_) => Ok(body.clone()),
        // Applications can be substituted recursively.
        SwailData::Apply(ref func, ref arg) => subs(func.deref(), val, depth)
            .and_then(|sub_func| subs(arg.deref(), val, depth)
            .map(|sub_arg| SwailData::Apply(Box::new(sub_func), Box::new(sub_arg))
        )),
        // Calls also need to be substituted in their arguments.
        SwailData::Call(ref call, ref continuation) => {
            match *call {
                SysCall::Exit(ref status) => subs(status.deref(), val, depth)
                    .map(|sub_status| SysCall::Exit(Box::new(sub_status))),
                SysCall::Write(ref bytes) => subs(bytes.deref(), val, depth)
                    .map(|sub_bytes| SysCall::Write(Box::new(sub_bytes))),
                SysCall::Read() => Ok(call.clone()),
                SysCall::Serialize(ref data) => subs(data.deref(), val, depth)
                    .map(|sub_data| SysCall::Serialize(Box::new(sub_data))),
                SysCall::Unserialize(ref bytes) => subs(bytes.deref(), val, depth)
                    .map(|sub_bytes| SysCall::Unserialize(Box::new(sub_bytes))),
                SysCall::Fork(ref code) => subs(code.deref(), val, depth)
                    .map(|sub_code| SysCall::Fork(Box::new(sub_code))),
                // TODO: implement everything else.
                _ => panic!("call substitution undefined"),
            }   .and_then(|sub_call| subs(continuation.deref(), val, depth)
                .map(|sub_cont| SwailData::Call(sub_call, Box::new(sub_cont))
            ))
        }
        // TODO: implement everything else.
        _ => panic!("substitution undefined"),
    }
}

// Function application for lambda terms.
fn apply(func: &SwailData, val: &SwailData)
        -> Result<SwailData, SwailError> {
    info!(target: "apply", "func: {:?} val: {:?}", func, val);
    match *func {
        SwailData::Lambda(ref body) => subs(body.deref(), val, 0)
            .and_then(|body_val| eval(&body_val)),
        _ => Err(SwailError::TypeError(SwailType::Lambda)),
    }
}

// The maximum number of bytes read in one go.
const READ_SIZE: usize = 256;

// Perform a system call with a continuation.
fn call(call: &SysCall, continuation: &SwailData)
        -> Result<SwailData, SwailError> {
    info!(target: "call", "call: {:?} continuation: {:?}", call, continuation);
    match *call {
        // TODO: threading
        SysCall::Exit(ref status) => eval(status.deref())
            .and_then(|status_val| Err(SwailError::StopExecution(status_val))
            ),
        SysCall::Write(ref bytes) => eval(bytes.deref())
            .and_then(|bytes_val| match bytes_val {
                SwailData::Bytes(data) => {
                    io::stdout().write_all(data.deref())
                        .map_err(|err| SwailError::FileError(err.kind()))
                        .and_then(|_| apply(continuation, &make_id()))
                }
                _ => Err(SwailError::TypeError(SwailType::Bytes)),
            }),
        SysCall::Read() => {
            let mut buffer = [0; READ_SIZE];
            io::stdin().read(&mut buffer[..])
                .map_err(|err| SwailError::FileError(err.kind()))
                .and_then(|count| apply(continuation, &make_bytes_buf(&buffer, count)))
        }
        SysCall::Serialize(ref data) => eval(data.deref())
            .and_then(|data_val| apply(
                continuation,
                &make_bytes_vec(serialize(&data_val))
        )),
        SysCall::Unserialize(ref bytes) => eval(bytes.deref())
            .and_then(|bytes_val| match bytes_val {
                SwailData::Bytes(slice) => unserialize(&mut Cursor::new(slice.deref()))
                    .map_err(|err| SwailError::BufferError(err.kind()))
                    .and_then(|data_val| apply(continuation, &data_val)),
                _ => Err(SwailError::TypeError(SwailType::Bytes)),
            }
        ),
        // TODO: implement everything else.
        _ => panic!("call undefined"),
    }
}

/*
// Perform a strict evaluation step on the data.
// (This is the small step of the samentics, and returns something to evaluate further.)
fn eval_step(data: &SwailData)
        -> Result<SwailData, SwailError> {

}
*/

// Perform strict full evaluation on the data.
// (This is the big step of the semantics, and returns a fully evaluated piece of data.)
pub fn eval(data: &SwailData)
        -> Result<SwailData, SwailError> {
    info!(target: "eval", "data: {:?}", data);
    match *data {
        // Lambdas get evaluated when applied to something.
        SwailData::Lambda(_) => Ok(data.clone()),
        // Variables can't be evaluated because then they're unbound.
        SwailData::Variable(_) => Err(SwailError::NameError()),
        // Assignments get evaluated by substituting the value into the lambda.
        SwailData::Apply(ref func, ref arg) => eval(func.deref())
            .and_then(|func_val| eval(arg.deref())
            .and_then(|arg_val| apply(&func_val, &arg_val))
            ),
        // Bytes get returned identically.
        SwailData::Bytes(ref bytes) => Ok(SwailData::Bytes(bytes.clone())),
        // Call is defined in the system call itself.
        SwailData::Call(ref syscall, ref continuation) => eval(continuation.deref())
            .and_then(|cont_val| call(syscall, &cont_val)
            ),
        // TODO: implement everything else.
        _ => panic!("evaluation undefined"),
    }
}

