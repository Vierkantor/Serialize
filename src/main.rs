#![feature(box_syntax, box_patterns)]

extern crate as_num;
extern crate byteorder;
extern crate chashmap;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate mio;
extern crate net2;
extern crate sodiumoxide;

use std::env;
use std::sync::{Arc};
use std::thread;

mod bytes;
mod crypto;
mod data;
use data::{
    SysCall,
    make_apply, make_call, make_lambda, make_variable
};
mod derived;
use derived::{make_const, make_id, make_Z};
mod eval;
use eval::{eval};
mod network;
use network::{make_listener, start_listening};
mod serialize;
mod threading;
mod zipper;

#[cfg(test)]
mod tests {
    use std::io::{Cursor};
    use std::ops::{Deref};
    use std::sync::{Arc};

    use crypto;
    use data::{SwailData, SwailError, SysCall,
        make_apply, make_bytes_buf, make_call, make_lambda, make_variable};
    use derived::{make_id};
    use eval::{eval};
    use serialize::{serialize, unserialize};
    use threading;
    #[test]
    fn eval_bytes() {
        let threads = threading::start();

        let bytes = [0x53, 0x77, 0x61, 0x69, 0x6c];
        let expr = make_bytes_buf(&bytes, 5);
        let result = eval(Arc::new(threads), expr.clone())
            .expect("evaluating the expression");
        assert_eq!(result, Box::new(expr));
    }
    #[test]
    fn eval_lambda() {
        let threads = threading::start();

        let body = make_variable(0);
        let expr = make_lambda(body);
        let result = eval(Arc::new(threads), expr.clone())
            .expect("evaluating the expression");
        assert_eq!(result, Box::new(expr));
    }
    #[test]
    fn eval_variable() {
        let threads = threading::start();

        let expr = make_variable(0);
        assert_eq!(eval(Arc::new(threads), expr), Err(SwailError::NameError()));
    }
    #[test]
    fn eval_apply_id() {
        let threads = threading::start();

        let bytes = [0x53, 0x77, 0x61, 0x69, 0x6C];
        let body = make_variable(0);
        let func = make_lambda(body);
        let val = make_bytes_buf(&bytes, 5);
        let expr = make_apply(func, val.clone());
        let result = eval(Arc::new(threads), expr)
            .expect("evaluating the expression");
        assert_eq!(result, Box::new(val));
    }
    #[test]
    fn apply_in_lambda() {
        let threads = threading::start();

        let bytes1 = make_bytes_buf(&[0x01], 1);
        let bytes2 = make_bytes_buf(&[0x02], 1);
        let constant = make_lambda(make_lambda(make_variable(1))); // \x y -> x
        let expr = make_apply(make_apply(constant, bytes1.clone()), bytes2);
        let result = eval(Arc::new(threads), expr).expect("evaluating the expression");
        assert_eq!(result, Box::new(bytes1));
    }
    #[test]
    fn serialize_unserialize() {
        let threads = Arc::new(threading::start());

        let bytes = [0x53, 0x77, 0x61, 0x69, 0x6C];
        let body = Box::new(SwailData::Variable(0));
        let func = Box::new(SwailData::Lambda(body));
        let val = Box::new(SwailData::Bytes(Box::new(bytes)));
        let expr = Box::new(SwailData::Apply(func, val.clone()));
        let result = eval(threads.clone(), expr.deref().clone())
            .expect("evaluating the expression");
        assert_eq!(result, val);
        let bytes = serialize(expr.deref());
        assert_eq!(vec![
            0x04,
                0x02, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05,
                    0x53, 0x77, 0x61, 0x69, 0x6C
        ], bytes);
        let expr2 = Box::new(unserialize(&mut Cursor::new(bytes.as_slice()))
            .expect("unserializing"));
        assert_eq!(expr2, expr);
        let result2 = eval(threads, expr2.deref().clone())
            .expect("evaluating expression 2");
        assert_eq!(result2, result);
    }

    #[test]
    fn encrypt_decrypt() {
        crypto::init();
        let threads = Arc::new(threading::start());

        let val = "Hello, World!".as_bytes();
        let bytes = make_bytes_buf(val, val.len());
        // VerifyDecrypted encrypted^0 sign_pub^2 encr_priv^3 => id
        let decr = make_call(
            SysCall::VerifyDecrypted(
                box make_variable(0),
                box make_variable(2),
                box make_variable(3),
            ),
            make_id()
        );
        // EncryptSigned bytes encr_pub^0 sign_priv^3 => λencrypted. decr
        let encr_decr = make_call(
            SysCall::EncryptSigned(
                box bytes.clone(),
                box make_variable(0),
                box make_variable(3),
            ),
            make_lambda(decr)
        );
        // ToPublic sign_priv^1 => λsign_pub. ToPublic encr_priv^1 => λencr_pub. encr_decr
        let split_keys = make_call(
            SysCall::ToPublic(box make_variable(1)),
            make_lambda(make_call(
                SysCall::ToPublic(box make_variable(1)),
                make_lambda(encr_decr)
            ))
        );
        // NewKey => λsign_priv. NewKey => λencr_priv. split_keys
        let expr = make_call(
            SysCall::NewKey(),
            make_lambda(make_call(SysCall::NewKey(), make_lambda(split_keys)))
        );

        let result = eval(threads.clone(), expr).expect("evaluating the expression");
        assert_eq!(result.as_ref(), &bytes);
    }
}

fn main() {
    env_logger::init().unwrap();
    crypto::init();

    // Read the server name from the arguments
    let args: Vec<_> = env::args().collect();
    let host_name;
    if args.len() > 1 {
        host_name = args[1].clone();
        info!("starting process with name {}", host_name);
    } else {
        panic!("could not determine a good host name");
    }

    let listener = make_listener(host_name).expect("could not start networking");
    let threads = Arc::new(threading::start());

    {
        let threads_ref = threads.clone();
        thread::spawn(move || {
            start_listening(listener, threads_ref)
        });
    }

    // λ msg. Write(msg) => const loop
    let print = make_lambda(make_call(SysCall::Write(box make_variable(0)),
        make_apply(make_const(), make_variable(1))));
    // λ loop . Recv() => print
    let recv = make_lambda(make_call(SysCall::Receive(), print));
    // Z recv id
    let expr = make_apply(make_apply(make_Z(), recv), make_id());

    println!("{:?}", eval(threads.clone(), expr));

    println!("Waiting for other threads to stop...");
    println!("(^C if you're impatient)");
    // TODO: actually join with the threads
    thread::park();
}
