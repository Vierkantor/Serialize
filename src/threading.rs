use chashmap::{CHashMap};
use std::sync::{Mutex};
use std::sync::mpsc::{Sender, Receiver, channel};

// The evaluation is correct, and we should do this next.
// In general, only eval_next should produce Higher or Finished.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum EvalState {
    Deeper,
    Higher,
    Finished,
}

// Represents the address a thread for message passing purposes.
pub type ThreadID = u64;
// Represents the messages sent between threads.
pub type Message = Vec<u8>;

// The information about a thread that others can see.
pub struct ThreadInfo {
    // TODO: to ensure sharing between threads, we put it into a Mutex,
    // but maybe figure out how to clone it when needed?
    pub messages: Mutex<Sender<Message>>,
}
// The information shared by all the threads,
// about which threads exist.
pub struct ThreadGlobal {
    next_id: Mutex<ThreadID>,
    threads: CHashMap<ThreadID, ThreadInfo>,
}
// The information about a thread that it can see.
#[derive(Debug)]
pub struct ThreadLocal {
    pub messages: Receiver<Message>,
    pub state: EvalState,
    pub thread_id: ThreadID,
}

// Start a new threading environment independent of others.
pub fn start() -> ThreadGlobal {
    ThreadGlobal {
        next_id: Mutex::new(0),
        threads: CHashMap::new(),
    }
}

// Get a new thread id, incrementing the global id.
pub fn new_thread_id(globals: &ThreadGlobal) -> ThreadID {
    let mut id = globals.next_id.lock().unwrap();
    let result = *id;
    *id += 1;
    result
}

// Create a new thread, inserting it into the global table.
pub fn new_thread(globals: &ThreadGlobal) -> ThreadLocal {
    // We use an asynchronous channel for two reasons:
    // * the buffer is unlimited, so we don't have to worry about it overlowing
    // * messages will be delivered in-order but without blocking the sending thread.
    let (tx, rx) = channel();
    let tid = new_thread_id(globals);
    let thread = ThreadLocal {
        messages: rx,
        state: EvalState::Deeper,
        thread_id: tid,
    };
    // Note that this goes spectacularly wrong if thread ids start to overlap.
    globals.threads.insert_new(tid, ThreadInfo {
        messages: Mutex::new(tx),
    });
    thread
}

// Send a message to the specified thread.
// If the specifed thread doesn't exist, no error is raised.
pub fn send_message(threads: &ThreadGlobal, thread_id: ThreadID, message: Message) {
    info!(target: "send_message", "thread_id: {:?} message: {:?}", thread_id, message);
    if let Some(thread_info) = threads.threads.get(&thread_id) {
        // TODO: handle unwrapping failures, maybe by pausing?
        let messages = thread_info.messages.lock().expect("could not lock message queue");
        // Ignore the error: it only occurs when the thread has died already,
        // and in that case we don't have to do anything.
        if let Err(_) = messages.send(message) {
            warn!(target: "send_message", "message to dead thread {:?}", thread_id);
        }
    } else {
        // Thread doesn't exist here, but that's okay.
        // We just act as if there is a thread that doesn't want to receive.
        warn!(target: "send_message", "message to unknown thread {:?}", thread_id);
    }
}

// Receive a message in the current thread.
// If there is no message, the thread blocks.
// If there are multiple messages, the first is returned.
pub fn receive_message(thread: &mut ThreadLocal) -> Message {
    info!(target: "receive_message", "thread: {:?}", thread);
    thread.messages.recv().expect("thread channel closed while thread is active")
}
