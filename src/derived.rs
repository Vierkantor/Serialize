use bytes::{Bytes};
use data::{
    SwailData, SysCall,
    make_apply, make_call, make_bytes_vec, make_lambda, make_variable,
};

// Give the identity function.
pub fn make_id() -> SwailData {
    make_lambda(make_variable(0))
}
// Give the constant function.
pub fn make_const() -> SwailData {
    make_lambda(make_lambda(make_variable(1)))
}

// Give the Z (fixpoint) combinator.
// (Similar to the Y combinator, but works under strict evaluation.
#[allow(non_snake_case)]
pub fn make_Z() -> SwailData {
    // inner = λv. x x v
    let inner = make_lambda(make_apply(make_apply(make_variable(1), make_variable(1)), make_variable(0)));
    // body = λx. f inner
    let body = make_lambda(make_apply(make_variable(1), inner));
    // λf. body body
    make_lambda(make_apply(body.clone(), body))
}

// Convert data representable as bytes to a byte array.
pub fn make_bytes<T>(data: T) -> SwailData
    where T: Bytes
{
    make_bytes_vec(data.to_bytes())
}

// Create a basic read-evaluate-print loop.
pub fn make_repl() -> SwailData {
    // λx. Write x => f
    let write_it = make_lambda(make_call(SysCall::Write(Box::new(make_variable(0))), make_variable(3)));
    // λx. Serialize x => write_it
    let repr_it = make_lambda(make_call(SysCall::Serialize(Box::new(make_variable(0))), write_it));
    // λb. Unserialize b => repr_it
    let eval_it = make_lambda(make_call(SysCall::Unserialize(Box::new(make_variable(0))), repr_it));
    // fix (λf. Read => eval_it)
    make_apply(make_Z(), make_lambda(make_call(SysCall::Read(), eval_it)))
}
