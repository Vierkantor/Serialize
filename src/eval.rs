use std::convert::{AsRef};
use std::io;
use std::io::{Cursor, Read, Write};
use std::sync::{Arc};
use std::thread;

use bytes::{Bytes};
use crypto::{Encrypted, PrivateKey, PublicKey, encrypt_signed, verify_decrypted};
use data::{
    Index, SwailData, SwailError, SwailType, SysCall,
    make_apply, make_bytes_buf, make_bytes_vec,
};
use derived::{make_id, make_bytes};
use serialize::{serialize, unserialize};
use threading::{
    EvalState, ThreadGlobal, ThreadLocal,
    new_thread, send_message, receive_message
};
use zipper::{Zipper, DataZipper, data_zipper};
use zipper::Movement::{Moved, Stayed};

// The maximum number of bytes read in one go.
const READ_SIZE: usize = 256;

// Convert a value from the given bytes, or produce an error.
fn from_bytes_erroring<B: Bytes>(bytes: SwailData) -> Result<B, SwailError> {
    match bytes {
        SwailData::Bytes(slice) => B::from_bytes(&mut Cursor::new(slice.as_ref()))
            .map_err(|err| SwailError::BufferError(err.kind())),
        _ => Err(SwailError::TypeError(SwailType::Bytes)),
    }
}

// Perform a system call with a continuation.
// The arguments to the call must be fully evaluated.
fn call(threads: Arc<ThreadGlobal>, thread: &mut ThreadLocal,
        call: SysCall, continuation: SwailData)
        -> Result<SwailData, SwailError> {
    info!(target: "call", "call: {:?} continuation: {:?}", call, continuation);
    match call {
        SysCall::Exit(status) => Err(SwailError::StopExecution(*status)),
        SysCall::Write(bytes) => {
            match *bytes {
                SwailData::Bytes(ref data) => {
                    io::stdout().write_all(data)
                        .map_err(|err| SwailError::FileError(err.kind()))
                        .map(|_| make_apply(continuation, make_id()))
                }
                _ => Err(SwailError::TypeError(SwailType::Bytes)),
            }
        }
        SysCall::Read() => {
            let mut buffer = [0; READ_SIZE];
            io::stdin().read(&mut buffer[..])
                .map_err(|err| SwailError::FileError(err.kind()))
                .map(|count|
                    make_apply(continuation, make_bytes_buf(&buffer, count))
                )
        }
        SysCall::Serialize(data) => Ok(
            make_apply(continuation, make_bytes_vec(serialize(data.as_ref())))
        ),
        SysCall::Unserialize(bytes) => {
            match *bytes {
                SwailData::Bytes(slice) => unserialize(&mut Cursor::new(slice.as_ref()))
                    .map_err(|err| SwailError::BufferError(err.kind()))
                    .map(|data_val| make_apply(continuation, data_val)),
                _ => Err(SwailError::TypeError(SwailType::Bytes)),
            }
        },
        SysCall::Fork(code) => {
            // TODO: implement some kind of green threading?
            let new_threads = threads.clone();
            let new_thread = new_thread(threads.as_ref());
            let tid = new_thread.thread_id;
            thread::spawn(move || {
                // TODO: monitoring
                let mut thread_info = new_thread;
                let thread_id = thread_info.thread_id;
                let _ = eval_thread(new_threads, &mut thread_info,
                    SwailData::Apply(code, box make_bytes(thread_id))
                );
            });
            Ok(make_apply(continuation, make_bytes(tid)))
        },
        SysCall::Send(tid, msg) => {
            match *tid {
                SwailData::Bytes(slice) => {
                    let mut cursor = Cursor::new(slice.as_ref());
                    Bytes::from_bytes(&mut cursor)
                        .map(|thread_id| {
                            send_message(threads.as_ref(), thread_id, serialize(&msg));
                            make_apply(continuation, make_id())
                        })
                        .map_err(|err| SwailError::BufferError(err.kind()))
                }
                _ => Err(SwailError::TypeError(SwailType::Bytes)),
            }
        },
        SysCall::Receive() => {
            let msg = receive_message(thread);
            unserialize(&mut Cursor::new(&msg))
                .map(|msg| make_apply(continuation, msg))
                .map_err(|err| SwailError::BufferError(err.kind()))
        }
        SysCall::ThreadID() => {
            Ok(make_apply(continuation, make_bytes(thread.thread_id)))
        }
        SysCall::NewKey() => {
            let key = PrivateKey::new();
            Ok(make_apply(continuation, make_bytes(key)))
        }
        SysCall::ToPublic(box key_bytes) => {
            let key: PrivateKey = from_bytes_erroring(key_bytes)?;
            Ok(make_apply(continuation, make_bytes(*key.to_public())))
        }
        SysCall::EncryptSigned(box data, box their_key_bytes, box our_key_bytes) => {
            match data {
                SwailData::Bytes(slice) => {
                    let their_key: PublicKey = from_bytes_erroring(their_key_bytes)?;
                    let our_key: PrivateKey = from_bytes_erroring(our_key_bytes)?;
                    let encr = encrypt_signed(slice.as_ref(), &their_key, &our_key);
                    Ok(make_apply(continuation, make_bytes(encr)))
                }
                _ => Err(SwailError::TypeError(SwailType::Bytes)),
            }
        }
        SysCall::VerifyDecrypted(box data, box their_key_bytes, box our_key_bytes) => {
            let encr: Encrypted = from_bytes_erroring(data)?;
            let their_key: PublicKey = from_bytes_erroring(their_key_bytes)?;
            let our_key: PrivateKey = from_bytes_erroring(our_key_bytes)?;
            // TODO: pass decryption error to the continuation
            // instead of just erroring out.
            let decr = verify_decrypted(encr, &their_key, &our_key)
                .expect("decryption error");
            Ok(make_apply(continuation, make_bytes_vec(decr)))
        }
    }
}

// Substitute the variable with given abstraction depth in the body.
// The value will be cloned for each time it is substituted.
fn subs(body: SwailData, val: &SwailData, depth: Index)
        -> Result<SwailData, SwailError> {
    info!(target: "subs", "body: {:?} val: {:?} depth: {:?}", body, val, depth);
    match body {
        // We have to go deeper!
        SwailData::Lambda(body) => subs(*body, val, depth + 1)
            .map(|sub_body| SwailData::Lambda(Box::new(sub_body))),
        // Substitute the right values.
        SwailData::Variable(var) => {
            if depth == var {
                Ok(val.clone())
            } else {
                Ok(SwailData::Variable(var))
            }
        }
        // Bytes can be passed identically.
        SwailData::Bytes(_) => Ok(body),
        // Applications can be substituted recursively.
        SwailData::Apply(box func, box arg) => subs(func, val, depth)
            .and_then(|sub_func| subs(arg, val, depth)
            .map(|sub_arg| SwailData::Apply(Box::new(sub_func), Box::new(sub_arg))
        )),
        // Calls also need to be substituted in their arguments.
        SwailData::Call(call, box continuation) => {
            match call {
                SysCall::Exit(status) => subs(*status, val, depth)
                    .map(|sub_status| SysCall::Exit(Box::new(sub_status))),
                SysCall::Write(bytes) => subs(*bytes, val, depth)
                    .map(|sub_bytes| SysCall::Write(Box::new(sub_bytes))),
                call@SysCall::Read() => Ok(call),
                SysCall::Serialize(data) => subs(*data, val, depth)
                    .map(|sub_data| SysCall::Serialize(Box::new(sub_data))),
                SysCall::Unserialize(bytes) => subs(*bytes, val, depth)
                    .map(|sub_bytes| SysCall::Unserialize(Box::new(sub_bytes))),
                SysCall::Fork(code) => subs(*code, val, depth)
                    .map(|sub_code| SysCall::Fork(Box::new(sub_code))),
                SysCall::Send(tid, msg) => {
                    subs(*tid, val, depth)
                    .and_then(move |sub_tid| subs(*msg, val, depth)
                    .map(|sub_msg| SysCall::Send(box sub_tid, box sub_msg)))
                },
                call@SysCall::Receive() => Ok(call),
                call@SysCall::ThreadID() => Ok(call),
                call@SysCall::NewKey() => Ok(call),
                SysCall::ToPublic(private) => subs(*private, val, depth)
                    .map(|sub_private| SysCall::ToPublic(box sub_private)),
                SysCall::EncryptSigned(data, their_key, our_key) => {
                    subs(*data, val, depth)
                        .and_then(move |sub_data| subs(*their_key, val, depth)
                        .and_then(move |sub_their| subs(*our_key, val, depth)
                        .map(|sub_our| SysCall::EncryptSigned(box sub_data, box sub_their, box sub_our)
                    )))
                },
                SysCall::VerifyDecrypted(data, their_key, our_key) => {
                    subs(*data, val, depth)
                        .and_then(move |sub_data| subs(*their_key, val, depth)
                        .and_then(move |sub_their| subs(*our_key, val, depth)
                        .map(|sub_our| SysCall::VerifyDecrypted(box sub_data, box sub_their, box sub_our)
                    )))
                },
            }   .and_then(|sub_call| subs(continuation, val, depth)
                .map(|sub_cont| SwailData::Call(sub_call, Box::new(sub_cont))
            ))
        }
    }
}

// Go to the next element in the evaluation order (i.e. depth-first left-to-right).
fn eval_next(thread: &mut ThreadLocal, data: DataZipper) -> DataZipper {
    match Zipper::right(data) {
        Moved(zip) => {
            thread.state = EvalState::Deeper;
            return zip;
        },
        Stayed(data) => match Zipper::up(data) {
            Moved(zip) => {
                thread.state = EvalState::Higher;
                return zip;
            }
            Stayed(data) => {
                thread.state = EvalState::Finished;
                return data;
            }
        }
    }
}

// Perform a strict evaluation step on the data.
// We need an indication in which direction we're going,
// to make sure function arguments get evaluated before function application.
// (This is the small step of the semantics, and returns something to evaluate further.)
// If there is nothing to evaluate, returns None.
fn eval_step(threads: Arc<ThreadGlobal>, thread: &mut ThreadLocal, data: DataZipper)
    -> Result<DataZipper, SwailError>
{
    // Exit if there's nothing to do.
    if thread.state == EvalState::Finished {
        return Ok(data);
    }
    let ctx = data.higher;
    match *data.lower {
        lambda@SwailData::Lambda(_) => Ok(eval_next(thread, DataZipper{
            higher: ctx,
            lower: box lambda,
        })),
        bytes@SwailData::Bytes(_) => Ok(eval_next(thread, DataZipper{
            higher: ctx,
            lower: box bytes,
        })),
        SwailData::Variable(_) => Err(SwailError::NameError()), // unbound variable
        apply@SwailData::Apply(_, _) => {
            match thread.state {
                EvalState::Deeper => return Ok(Zipper::down(DataZipper{
                        higher: ctx,
                        lower: box apply,
                    }).expect("can't move down on Apply")),
                EvalState::Higher => (),
                EvalState::Finished => panic!("attempt to evaluate after finishing"),
            }
            // We're in the Higher state, so it's time to do function application.
            match apply {
                SwailData::Apply(box SwailData::Lambda(box body), box x) => {
                    thread.state = EvalState::Deeper;
                    subs(body, &x, 0)
                    .map(|val| data_zipper(ctx, val))
                },
                // We need a lambda to apply, otherwise it doesn't make sense.
                _ => Err(SwailError::TypeError(SwailType::Lambda)),
            }
        }
        full_call@SwailData::Call(_, _) => {
            match thread.state {
                EvalState::Deeper => Ok(Zipper::down(DataZipper{
                        higher: ctx,
                        lower: box full_call,
                    }).expect("can't move down on Call")),
                EvalState::Higher => {
                    if let SwailData::Call(syscall, cont) = full_call {
                        thread.state = EvalState::Deeper;
                        call(threads, thread, syscall, *cont)
                            .map(|result| data_zipper(ctx, result))
                    } else {
                        panic!("attempt to call to non-syscall");
                    }
                },
                EvalState::Finished => panic!("attempt to evaluate after finishing"),
            }
        }
    }
}

// Perform strict full evaluation on the data.
// (This is the big step of the semantics, and returns a fully evaluated piece of data.)
//
// The data is evaluated in the given thread context.
fn eval_thread(threads: Arc<ThreadGlobal>, mut thread: &mut ThreadLocal, data: SwailData)
    -> Result<Box<SwailData>, SwailError>
{
    info!(target: "eval_thread", "data: {:?} thread: {:?}", data, thread);
    let mut cur_data: Box<DataZipper> = Box::new(Zipper::zip(Box::new(data)));
    loop {
        let new_data = eval_step(threads.clone(), &mut thread, *cur_data)?;
        match thread.state {
            EvalState::Finished => {
                info!("execution of thread {:?} finished", thread.thread_id);
                return Ok(Zipper::unzip(new_data));
            }
            _ => (),
        };
        cur_data = Box::new(new_data);
    };
}

// Perform strict full evaluation on the data.
// (This is the big step of the semantics, and returns a fully evaluated piece of data.)
//
// The data is evaluated in a new thread context,
// but you need to manually start a thread if you need concurrency.
// We need the thread globals to be Arc to ensure they survive
// in child threads when this one dies.
pub fn eval(threads: Arc<ThreadGlobal>, data: SwailData)
    -> Result<Box<SwailData>, SwailError>
{
    info!(target: "eval", "data: {:?}", data);
    let mut thread = new_thread(threads.as_ref());
    eval_thread(threads, &mut thread, data)
}
