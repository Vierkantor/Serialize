use sodiumoxide;
use sodiumoxide::crypto::{box_};
use std::io;
use std::io::{Cursor, ErrorKind, Read};

use bytes::{Bytes, SerializeError};

// Used to encrypt and verify data.
// You can share this freely.
pub type PublicKey = box_::PublicKey;

// Used to decrypt and sign data.
// (Also includes the public component for easier operations.)
// You should keep this within some trusted region of code.
#[derive(Clone, Debug, PartialEq)]
pub struct PrivateKey {
    public: box_::PublicKey,
    secret: box_::SecretKey,
}

impl PrivateKey {
    // Create a new private key.
    pub fn new() -> PrivateKey {
        let (public, secret) = box_::gen_keypair();
        PrivateKey {
            public: public,
            secret: secret,
        }
    }

    // Get a public key from a private key.
    pub fn to_public<'a>(self: &'a Self) -> &'a PublicKey {
        &self.public
    }
}

impl Bytes for PublicKey {
    fn to_bytes(self) -> Vec<u8> {
        // TODO: we assume the key format doesn't change.
        let mut result = Vec::with_capacity(box_::PUBLICKEYBYTES);
        result.extend_from_slice(&self[..]);
        result
    }
    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<PublicKey> {
        let mut pub_buf = [0; box_::PUBLICKEYBYTES];
        buf.read_exact(&mut pub_buf)?;
        let maybe_result = box_::PublicKey::from_slice(&pub_buf);
        if let Some(result) = maybe_result {
            Ok(result)
        } else {
            Err(io::Error::new(ErrorKind::Other, SerializeError::FormatError))
        }
    }
}
impl Bytes for PrivateKey {
    fn to_bytes(self) -> Vec<u8> {
        // TODO: we assume the key format doesn't change.
        let mut result = Vec::with_capacity(box_::SECRETKEYBYTES + box_::PUBLICKEYBYTES);
        result.extend_from_slice(&self.public[..]);
        result.extend_from_slice(&self.secret[..]);
        result
    }
    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<PrivateKey> {
        let mut secr_buf = [0; box_::SECRETKEYBYTES];
        let mut pub_buf = [0; box_::PUBLICKEYBYTES];
        buf.read_exact(&mut pub_buf)?;
        buf.read_exact(&mut secr_buf)?;
        let maybe_result = box_::PublicKey::from_slice(&pub_buf)
            .and_then(|public| box_::SecretKey::from_slice(&secr_buf)
            .map(|secret| PrivateKey { public: public, secret: secret }
        ));
        if let Some(result) = maybe_result {
            Ok(result)
        } else {
            Err(io::Error::new(ErrorKind::Other, SerializeError::FormatError))
        }
    }
}

// Represents an encrypted message.
#[derive(Clone, Debug, PartialEq)]
pub struct Encrypted {
    ciphertext: Vec<u8>,
    nonce: box_::Nonce,
}

impl Bytes for Encrypted {
    fn to_bytes(self) -> Vec<u8> {
        // TODO: we assume the nonce format doesn't change.
        let mut result = Vec::with_capacity(box_::NONCEBYTES);
        result.extend_from_slice(&self.nonce[..]);
        result.append(&mut self.ciphertext.len().to_bytes());
        let mut ciphertext = self.ciphertext;
        result.append(&mut ciphertext);
        result
    }
    fn from_bytes(buf: &mut Cursor<&[u8]>) -> io::Result<Encrypted> {
        let mut nonce_buf = [0; box_::NONCEBYTES];
        buf.read_exact(&mut nonce_buf)?;

        let length = usize::from_bytes(buf)?;
        let mut cipher_buf = vec![0; length];
        buf.read_exact(&mut cipher_buf)?;

        let maybe_nonce = box_::Nonce::from_slice(&nonce_buf);
        if let Some(nonce) = maybe_nonce {
            Ok(Encrypted {
                nonce: nonce,
                ciphertext: cipher_buf,
            })
        } else {
            Err(io::Error::new(ErrorKind::Other, SerializeError::FormatError))
        }
    }
}

// Initialize the cryptography features.
// This is the only member of the module that should be used by only one thread,
// any result afterward may be freely shared.
pub fn init() {
    sodiumoxide::init();
}

// Sign and encrypt data.
// The data will be encrypted using their (public) key,
// and signed using our (private) key.
//
// The result can be passed to verify_decrypted,
// with the private and public key swapped,
// to get the original data back.
pub fn encrypt_signed(data: &[u8], their_key: &PublicKey, our_key: &PrivateKey)
    -> Encrypted
{
    let nonce = box_::gen_nonce();
    Encrypted {
        ciphertext: box_::seal(data, &nonce, their_key, &our_key.secret),
        nonce: nonce,
    }
}

// Verify and decrypt data.
// The data will be verified using their (public) key,
// and decrypted using our (private) key.
// If the data does not pass verification, None is returned.
//
// The ciphertext can be obtained from encrypt_signed.
// with the private and public key swapped.
pub fn verify_decrypted(data: Encrypted, their_key: &PublicKey, our_key: &PrivateKey)
    -> Option<Vec<u8>>
{
    box_::open(&data.ciphertext, &data.nonce, their_key, &our_key.secret).ok()
}

mod tests {
    use crypto::*;

    // Encrypt something and decrypt it again,
    // which should give the original data.
    #[test]
    fn roundtrip() {
        init();
        let data = "Hello, World!".as_bytes();
        let sign_key = PrivateKey::new();
        let encr_key = PrivateKey::new();
        let encr = encrypt_signed(data, &encr_key.to_public(), &sign_key);
        let decr = verify_decrypted(encr, &sign_key.to_public(), &encr_key)
            .expect("verification failed");
        assert_eq!(decr, data);
    }

    // Encrypt something and decrypt it with another key,
    // which should give an error.
    #[test]
    fn wrong_encr_key() {
        init();
        let data = "Hello, World!".as_bytes();
        let sign_key = PrivateKey::new();
        let encr_key = PrivateKey::new();
        let decr_key = PrivateKey::new();
        let encr = encrypt_signed(data, &encr_key.to_public(), &sign_key);
        let decr = verify_decrypted(encr, &sign_key.to_public(), &decr_key);
        assert_eq!(decr, None);
    }

    // Encrypt something and verify it with another key,
    // which should give an error.
    #[test]
    fn wrong_sign_key() {
        init();
        let data = "Hello, World!".as_bytes();
        let sign_key = PrivateKey::new();
        let encr_key = PrivateKey::new();
        let vald_key = PrivateKey::new();
        let encr = encrypt_signed(data, &encr_key.to_public(), &sign_key);
        let decr = verify_decrypted(encr, &vald_key.to_public(), &encr_key);
        assert_eq!(decr, None);
    }

    #[test]
    fn private_key_bytes() {
        init();
        let key = PrivateKey::new();
        let key_bytes = key.clone().to_bytes();
        let key2 = PrivateKey::from_bytes(&mut Cursor::new(&key_bytes))
            .expect("when reading key from bytes");
        assert_eq!(key, key2);
    }
    #[test]
    fn public_key_bytes() {
        init();
        let priv_key = PrivateKey::new();
        let key = priv_key.to_public();
        let key_bytes = key.clone().to_bytes();
        let key2 = PublicKey::from_bytes(&mut Cursor::new(&key_bytes))
            .expect("when reading key from bytes");
        assert_eq!(key, &key2);
    }
    #[test]
    fn encrypted_bytes() {
        init();
        let data = "Hello, World!".as_bytes();
        let sign_key = PrivateKey::new();
        let encr_key = PrivateKey::new();
        let encr = encrypt_signed(data, &encr_key.to_public(), &sign_key);
        let encr_bytes = encr.clone().to_bytes();
        let encr2 = Encrypted::from_bytes(&mut Cursor::new(&encr_bytes))
            .expect("when reading encrypted data from bytes");
        assert_eq!(encr, encr2);
    }
}
