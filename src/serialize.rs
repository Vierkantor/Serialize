use as_num::{AsNum};
use std::io;
use std::io::{Cursor, Read};
use std::ops::{Deref};

use bytes::{Bytes};
use data::{SwailData, SysCall};

// The format is built of a one-byte header, followed by the relevant data.
const BYTES_PER_INT: usize = 8; // int means 8 big-endian bytes, i.e. u64.
const INVALID_HEADER: u8 = 0x00; // Marks that the data is invalid.
const BYTES_HEADER: u8 = 0x01; // <1 int: length> <length bytes: data>
const LAMBDA_HEADER: u8 = 0x02; // <data: lambda body>
const VARIABLE_HEADER: u8 = 0x03; // <1 int: index>
const APPLY_HEADER: u8 = 0x04; // <data: function> <data: argument>
const CALL_HEADER: u8 = 0x05; // <1 byte: call id> <data: continuation> ...

fn call_number(call: &SysCall) -> u64 {
    match *call {
        SysCall::Exit(_) => 0x00,
        SysCall::Write(_) => 0x01,
        SysCall::Read() => 0x02,
        SysCall::Serialize(_) => 0x03,
        SysCall::Unserialize(_) => 0x04,
        SysCall::Fork(_) => 0x05,
        SysCall::Send(_, _) => 0x06,
        SysCall::Receive() => 0x07,
        SysCall::ThreadID() => 0x08,
        SysCall::NewKey() => 0x09,
        SysCall::ToPublic(_) => 0x0a,
        SysCall::EncryptSigned(_, _, _) => 0x0b,
        SysCall::VerifyDecrypted(_, _, _) => 0x0c,
    }
}

// Convert data to bytes.
pub fn serialize(data: &SwailData) -> Vec<u8> {
    match *data {
        SwailData::Bytes(ref bytes) => {
            let mut result = vec![BYTES_HEADER];
            result.extend(bytes.len().to_bytes());
            result.extend(bytes.iter().cloned());
            result
        }
        SwailData::Lambda(ref body) => {
            let mut result = vec![LAMBDA_HEADER];
            result.append(&mut serialize(body.deref()));
            result
        }
        SwailData::Variable(ref var) => {
            let mut result = vec![VARIABLE_HEADER];
            result.append(&mut var.to_bytes());
            result
        }
        SwailData::Apply(ref func, ref val) => {
            let mut result = vec![APPLY_HEADER];
            result.append(&mut serialize(func.deref()));
            result.append(&mut serialize(val.deref()));
            result
        }
        SwailData::Call(ref call, ref continuation) => {
            let mut result = vec![CALL_HEADER];
            result.append(&mut call_number(call).to_bytes());
            result.append(&mut serialize(continuation.deref()));
            match *call {
                SysCall::Exit(ref status) => {
                    result.append(&mut serialize(status.deref()))
                }
                SysCall::Write(ref bytes) => {
                    result.append(&mut serialize(bytes.deref()))
                }
                SysCall::Read() => {} // No arguments
                SysCall::Serialize(ref data) => {
                    result.append(&mut serialize(data.deref()))
                }
                SysCall::Unserialize(ref bytes) => {
                    result.append(&mut serialize(bytes.deref()))
                }
                SysCall::Fork(ref code) => {
                    result.append(&mut serialize(code.deref()))
                }
                SysCall::Send(ref thread_id, ref message) => {
                    result.append(&mut serialize(thread_id.deref()));
                    result.append(&mut serialize(message.deref()))
                }
                SysCall::Receive() => {}
                SysCall::ThreadID() => {}
                SysCall::NewKey() => {}
                SysCall::ToPublic(ref key) => {
                    result.append(&mut serialize(&key));
                }
                SysCall::EncryptSigned(ref data, ref their_key, ref our_key) => {
                    result.append(&mut serialize(&data));
                    result.append(&mut serialize(&their_key));
                    result.append(&mut serialize(&our_key));
                }
                SysCall::VerifyDecrypted(ref data, ref their_key, ref our_key) => {
                    result.append(&mut serialize(&data));
                    result.append(&mut serialize(&their_key));
                    result.append(&mut serialize(&our_key));
                }
            }
            result
        }
    }
}

// Get a system call from bytes.
fn unserialize_call(mut bytes: &mut Cursor<&[u8]>, number: u64)
        -> io::Result<SysCall> {
    match number {
        0x00 => {
            let status = unserialize(bytes)?;
            Ok(SysCall::Exit(Box::new(status)))
        }
        0x01 => {
            let text = unserialize(bytes)?;
            Ok(SysCall::Write(Box::new(text)))
        }
        0x02 => {
            Ok(SysCall::Read())
        }
        0x03 => {
            let data = unserialize(bytes)?;
            Ok(SysCall::Serialize(Box::new(data)))
        }
        0x04 => {
            let data = unserialize(bytes)?;
            Ok(SysCall::Unserialize(Box::new(data)))
        }
        0x05 => {
            let code = unserialize(bytes)?;
            Ok(SysCall::Fork(Box::new(code)))
        }
        0x06 => {
            let thread_id = unserialize(bytes)?;
            let message = unserialize(bytes)?;
            Ok(SysCall::Send(box thread_id, box message))
        }
        0x07 => {
            Ok(SysCall::Receive())
        }
        0x08 => {
            Ok(SysCall::ThreadID())
        }
        0x09 => {
            Ok(SysCall::NewKey())
        }
        0x0a => {
            let key = unserialize(bytes)?;
            Ok(SysCall::ToPublic(box key))
        }
        0x0b => {
            let data = unserialize(bytes)?;
            let their_key = unserialize(bytes)?;
            let our_key = unserialize(bytes)?;
            Ok(SysCall::EncryptSigned(box data, box their_key, box our_key))
        }
        0x0c => {
            let data = unserialize(bytes)?;
            let their_key = unserialize(bytes)?;
            let our_key = unserialize(bytes)?;
            Ok(SysCall::VerifyDecrypted(box data, box their_key, box our_key))
        }
        _ => panic!("unknown call id"),
    }
}

// Get data from bytes
pub fn unserialize(mut bytes: &mut Cursor<&[u8]>) -> io::Result<SwailData> {
    info!("unserialize bytes: {:?}", bytes);
    let mut header: [u8; 1] = [INVALID_HEADER];
    bytes.read_exact(&mut header)?;
    match header[0] {
        INVALID_HEADER => Err(io::Error::new(io::ErrorKind::Other, "invalid header")),
        BYTES_HEADER => {
            let length: usize = Bytes::from_bytes(&mut bytes)?;
            let start: usize = bytes.position().as_num();
            let end = start + length;
            bytes.set_position(end.as_num());
            Ok(SwailData::Bytes(Box::from(&bytes.get_ref()[start..end])))
        },
        LAMBDA_HEADER => {
            let body = unserialize(bytes)?;
            Ok(SwailData::Lambda(Box::new(body)))
        },
        APPLY_HEADER => {
            let func = unserialize(bytes)?;
            let arg = unserialize(bytes)?;
            Ok(SwailData::Apply(Box::new(func), Box::new(arg)))
        },
        VARIABLE_HEADER => {
            let index = u64::from_bytes(bytes)?;
            Ok(SwailData::Variable(index))
        },
        CALL_HEADER => {
            let number: u64 = u64::from_bytes(bytes)?;
            let callback = unserialize(bytes)?;
            let call = unserialize_call(bytes, number)?;
            Ok(SwailData::Call(call, Box::new(callback)))
        }
        _ => {
            warn!("unknown header number {}", header[0]);
            Err(io::Error::new(io::ErrorKind::Other, "unknown header"))
        },
    }
}

