This is a minimalistic implementation of a programming language, inspired by languages such as FORTH and Unlambda. The goal is to build a more complicated thing on this simple basis.

# Language specification
This is the specification of the language, which you can read to understand how to make your own implementation. It's probably not a very good reference for when you want to program, so feel free to write one!

## Meta-Semantics
Each implementation should be accompanied with a language reference that details how the implementation extends this specification.

When an aspect of the semantics is *implementation-defined*, each implementation must define in its documentation how the implementation handles this case. If a language reference is included, this definition should be included in this reference. Each implementation should be consistent in its handling of the case.

It is implementation-defined whether an implementation performs any analysis or operations (e.g. type checking and optimalization) on the program before execution. Of course, this pre-execution phase must not have any impact on the semantics of the program.

Any component in the program that is *incorrect* according to the semantics, is handled in an implementation-defined manner. You can generally recognize incorrect constructs in the specification by the words "it is an error to". A recommended way is to print a human-readable message to the standard output as soon as the incorrectness is detected, and to stop execution. Because the presence or absence of a pre-execution phase depends on the implementation, a program with incorrect components may execute until no more of the program can be executed, or it may not even start execution. You probably shouldn't rely on errors not occurring to perform optimizations, as that really tends to mislead programmers.

## Syntax
The only syntax defined is a binary format for serializing and unserializing expressions from a byte array. Each expression in the binary format has one byte header, which defines the pattern of the expression. (Therefore, any zero-byte piece of data is invalid.) Note that each value during execution (i.e. the values that can be passed to Serialize) must have a representation. Runtime representation of values does not need to match with serialized representation; the implementation is free to optimize the data it encounters.

An int consists of 8 big-endian bytes. A piece of data is an expression, encoded in the same syntax as we're defining.

Header byte | Body bytes                            | Semantics
-----------:|---------------------------------------|---------------------
       0x00 | N/A                                   | Incorrect header
       0x01 | (length: int) (data: byte[length])    | Bytes literal
       0x02 | (body: data)                          | Lambda term
       0x03 | (index: int)                          | Variable, De Bruijn-indexed
       0x04 | (function: data) (argument: data)     | Function application
       0x05 | (call: int) (continuation: data) ...  | System call
  0x06-0x7F | N/A                                   | Reserved
  0x80-0xFF | N/A                                   | Implementation-defined

It is an error to have data with header `0x00`. It is an error for the byte array to be shorter than the indicated length, especially in the case when the whole array has length 0.

If your implementation uses a custom header, you might want to devise a versioning scheme using the 0x80 header. In case this is a commonly used construct, the specification will make a specific edition the official one, so please come up with some good suggestions.

Bytes literals have a maximum length of 2^63 - 1 = 9223372036854775807 = 0x7FFFFFFFFFFFFFFF. This has a couple of useful consequences:

* Implementations using unsigned integers should always report the error.
* If in the future 2^63 bytes are not enough for anybody, we can implement even longer literals using the top bit.

## Basic evaluation
The semantics of a program determine the effects during execution, and the final evaluated result. We will use the abstract syntax of the previous section to denote semantics, but an implementation may use any other equivalent representation of the program during execution, in particular some form of machine code.

Note that all data is considered immutable. The way the implementation handles updates, in particular sharing, should be consistent with a completely immutable system which always copies the full data on update.

When a bytes literal is executed, no side-effect occurs and the result is the literal.

When a lambda term is executed, no side-effect occurs and the result is the term.

It is an error to execute a variable. (Because all variables must be bound by some lambda, and hence substituted before they get evaluated.)

A function application is executed by first executing the function, then the argument, and then substituting the evaluated argument in the evaluated function. Finally, the substitution result is executed. The evaluation result is the result of the final execution.

The result of substituting a value into a lambda term is the body of the lambda term, with every occurence of the variable bound by the lambda, replaced by the value.

## Intermezzo: data types and Church encoding
At this point, our language is equivalent to the lambda calculus, and as such is Turing-complete already. This is a bit too abstract a claim for practical purposes, so it's a good idea to check how to write some common features in it.

Non-recursive functions of one parameter with variables and application is something we start with, and we can build the remainder with that.

The easiest way to start is to make multiple-parameter functions. The trick here is to realize that we have closures: if you have a lambda that returns a lambda, the return value may still refer to variables bound in the outer lambda. So a multi-parameter function λ (x, y) . t : (A, B) -> C can be converted to a function λ x . λ y . t : A -> (B -> C) that takes one parameter and stores it in the closure of the lambda that takes the second parameter. (It's no accident that people write the tuple type (A, B) as a product AB, and function types A -> B as an exponent B^A, and that for all numbers a, b, c we have a^(bc) = (a^b)^c. DO-TIP: count how many functions there are from a finite set A to a finite set B.)

To make a recursive function, you can start with the Y combinator. This is a higher-order function that applies a function to (itself applied to (itself applied to (itself ...))). The Y combinator is given by the lambda term λf. (λx. f (x x)) (λ x. f (x x)). (You can check that evaluating this term one step gives f ((λx. f (x x)) (λ x. f (x x))), evaluating twice gives f f ((λx. f (x x)) (λ x. f (x x))) and so on.) The serialized edition of this λ-term is:

```
0x02 0x04 0x02 0x04 0x03 0x0000000000000001 0x04 0x03 0x0000000000000000 0x03 0x0000000000000000 0x02 0x04 0x03 0x0000000000000001 0x04 0x03 0x0000000000000000 0x03 0x0000000000000000
```

The trick is to write a recursive function with one more parameter, which will be filled by the function to recurse into, by the Y combinator. So the factorial function can be written as Y (λ fac' . λ n . n * fac' n). This is great in theory, except that this doesn't work with strict evaluation. This version does work: λ f. (λ x. f (λv. (x x) v))(λ x. f (λv. (x x) v)). And then there is also the problem of implementing *mutually* recursive functions, where each function can call one of the others. You can read more about implementing those [on Stackoverflow](https://stackoverflow.com/questions/4899113/fixed-point-combinator-for-mutually-recursive-functions).

The other thing we can define are algebraic datatypes. The things we need for algebraic datatypes are ways to construct sums and products, ways to pattern-match on sums, and a way to get components from the product. It's easier if we start with product types.

A product type is just a fancy way to write a tuple, so an expression that uses the different fields of a product type is just a function that takes a tuple of arguments. But that's the same as a function with multiple parameters, and we know how to write those. So for a product of types A, B, their destructuring function has a type ∀c. (A, B) -> (A -> B -> c) -> c.

What really happens when you pattern-match? You have a sum value S : T_1 + ... + T_n, and a collection of code C_1, ..., C_n to execute, one for each alternative. Each C_i has a free variable that we fill in with the corresponding alternative of S, so if we want the match to have a type A, the type of a C_i is T_i -> A. So pattern matching for the sum of types A and B has a type ∀c (A + B) -> (A -> c) -> (B -> c) -> c.

The trick we do now is to say that a sum type and product type is exactly their destructor / matcher respectively, so we don't need a special destruct/match feature! Therefore, we will translate the tuple (x, y) as λf. f x y, the alternative Inl x as λf g. f x and the alternative Inr y as λf g. g y.

So what about enums? A (finite) enumeration is just a choice between a set of n options, so let's write it as a type n. Now if we define that 1 is the special unit type, such that the product A × 1 = 1 × A = A for all types A, this should be the type that takes a function, applies *no* arguments to it, and returns it: the identity function. We can abuse notation (remember the *algebraic* part of algebraic data types?) to get n = n * 1 = 1 + 1 + ... + 1, i.e. an enum can be represented by the type of all functions that take n arguments and return one of them.

(For curious people: the zero type, which is the additive identity with A + 0 = 0 + A = A for all A, is then the sum of no alternatives, i.e. given no arguments at all, it comes up with a value of an arbitrary type. You can imagine why this is the representation of errors in popular programming languages.)

## System calls
System calls are a more complicated construct, as they are our main extension of the lambda calculus. A system call expression has one expression as parameter. This is the continuation, a lambda term that will be applied to the result of the system call. (If you want to understand this, you can check out the related concepts of continuations, continuation passing style and call/cc.)

When a system call is executed, first the continuation is executed. If the evaluated continuation is not a lambda term, an error occurs. Then any parameters, as defined in the table below, are evaluated in left-to-right order (these are numbered 1..n, since the continuation is 0). Finally, the system call performs an operation as defined in the table below. The result of executing a system call is different from the value that the continuation is applied to. (TODO: decide what to do there, maybe just the return value of the continuation by default?)

Call number | Call name   | Parameters                         | Semantics
-----------:|-------------|------------------------------------|----------
       0x00 | Exit        | {status: data}                     | Stop the currently executing thread. The status is passed to any monitors of the thread. The continuation or any surrounding code will not be executed further.
       0x01 | Write       | {text: bytes}                      | Write the text to the implementation-defined output stream (e.g. stdout or a log). The continuation is applied to an implementation-defined value.
       0x02 | Read        | {}                                 | Read bytes from the implementation-defined input stream (e.g. stdin or source file). The continuation is applied to this byte array.
       0x03 | Serialize   | {value: data}                      | Convert the value to bytes in the syntax defined above. This operation is not idempotent: serializing a byte array adds a data header. The continuation is applied to the resulting byte array.
       0x04 | Unserialize | {data: bytes}                      | Convert bytes to data in the syntax defined above. This operation is not idempotent: unserializing a serialized byte array removes a data header. The continuation is applied to the resulting data.
       0x05 | Fork        | {code: lambda}                     | Create a new thread that executes the code. The continuation and code are passed an implementation-defined thread identifier.
       0x06 | Send        | {receiver: thread} {message: data} | Send a message to the specified thread. The message will be read by the receiving thread using Receive. It is implementation-defined whether the current thread can be paused, e.g. when the receiver has a full message queue. The continuation is applied to an implementation-defined value.
       0x07 | Receive     | {}                                 | Receive a message sent to the current thread by the Send call. If no message has been sent yet, the current thread is paused. If multiple messages have been sent, the first unreceived message is returned, so calling Receive repeatedly will eventually encounter all messages. The continuation is applied to the data received.
       0x08 | Thread ID   | {}                                 | Get the thread ID of the thread this call is performed in. The continuation is applied to this thread identifier.

## Threading model
The threading model is inspired by Erlang. Because everything is basically a pure function, we have parallelism for free. The only way to have threads influence each other is to have one send a message to the other, which gives nice guarantees of atomicity.

Threads can be *running*, *paused* or *completed*. Each computation is executed in a running thread (the *current thread* for the code). Conversely, each (living) thread contains code that is being executed. Any program passed to the implementation is executed in one thread that starts running. Any running thread can start new threads using the Fork system call. The new thread starts executing the code passed to Fork, and continues the execution until the value is fully reduced, the call Exit is called, or an error occurs. Then, the thread is completed and its final value (or the argument to Exit) is stored.

When a thread still has code to execute, but depends on other threads for a result, it will be paused. An implementation may run all running threads in parallel or all in sequence, as long as there is weak fairness: at each moment, if there is a non-paused thread, something will be executed. Note that this does not imply strong fairness: if one thread is executing an infinite loop, another thread may not necessarily be allowed to execute.

Threads can communicate by sending each other messages. Each thread has an identifier (whose format is implementation-defined), passed to threads in the Fork and Thread ID calls. This identifier uniquely specifies which thread gets which message. Multiple IDs should not point to the same thread, but if this occurs anyway, the implementation should behave as if they refer to separate threads that happen to execute the same code (and whose side effects occur only once). It is not an error to send to a thread that isn't receiving or in fact a thread that does not exist.

Messages are handled consistently: when multiple threads send a message or receive one, the result is identical to one given by executing everything sequentially. Importantly, this sequentializing may not reorder execution inside a thread, or the order of a Send-Receive pair of a message.

Except for explicitly mentioned, code should be thread-agnostic: executing code in one thread and sending its result should be (basically) the same as sending the code and executing it in another thread. The specification will be vague for now, as there are quite a few edge cases, but in general only system calls that refer to the status of a thread (e.g. Exit, Send, Receive and Thread ID) should be affected. Note that this also applies to implementation-defined values such as thread IDs: if your implementation allows distribution of threads over a network, it must ensure that thread IDs can be passed over the network transparently as well.

# Implementation details
To make the implementation light-weight, there is no pre-execution checking phase. As a result, errors are generally reported at execution time when the erroring construct is encountered. When an error occurs, execution is stopped, the error and some more relevant information is written to stderr. Text is fully parsed when unserialized, even when a subexpression is never used.

There are no implementation-defined data headers, only those defined in the language itself.

The output stream is stdout, and input stream is stdin. Text is encoded as UTF-8 in these streams, and may contain 0x00 bytes.

The thread identifier is (currently) represented as an int packed into a byte array. This will probably change as it gets networking capability, to ensure they remain unique over hosts.

# Distributivity
The one big remaining thing to note is how to do distributed computing. After all, it's not much use specifying a serialisation format if you can't do anything with those bytes. However, this part of the language is quite implementation-specific and may be reworked, so it's not part of the spec yet.

The goal of this design is to make inter-process communication as easy as in-process communication. Therefore, we should be able to treat each thread basically identically, no matter where it is located.

Important consequences:

* Security is hard! This is not really a direct consequence, but everyone should keep remembering this.
* We can use encryption to avoid eavesdroppers.
* We can use certificates to avoid impersonation (though we need a good way to verify them).
* With authentication, we can also avoid any accidental damage to our messages by the network.
* Once you accept someone's messages, they can basically send you anything to execute.
* All threads communicate asynchronously.
* There is no way to ensure a thread is killed (since your network may go down in the meantime).
* All data must be convertible from a process-(or even machine-)specific format (e.g. pointers or local paths).

The first thing we make sure is that inter-process communication happens under some form of encryption.
Processes should have some way of advertising themselves to the public, which has to happen in plain view.
It's very impolite to advertise as another process, but we explicitly don't really deal with this.
Instead, we just ensure that the code that receives a message is the intended recipient, see below.
This also ensures that a failing host cannot really take out your system, instead you get another host to accomplish the same service.

Once we are sure that messages sent are messages received, we are still quite far from being a nice distributed model of computing.
When we want to send a message, where should we send the messages?
When we're receiving a message, how can we be sure the sender isn't doing evil things?
(Of course, we also get the problem of trusting trust, but as you're probably reading this from a computer screen, not on a piece of paper personally handed down by myself, let's not get into that.

To address these questions, we should probably introduce a concept of *service*, where a service is a bunch of (running) code that "works as a team", even though it may be distributed over a lot of threads or processes or hosts.
This will allow us to distinguish between service-local and inter-service trust.
Code that belongs to one service should be able to trust all code that belongs to that service (and probably also any child services it launches?), except for some exceptions like high-security encryption-like stuff that you really need secured.

It would be neat to sign/encrypt messages using some PGP-like system. Maybe it can work like the following idea.
Each thread has a private/public keypair, and its public key identifies the thread uniquely.
On forking, the parent and child thread are both passed the other's public key. On initializing a process, its parent key is the starting user's key.
You make a key pair when you design a service, and send it to any instance of the service you start up. (Note the difference between designing and starting: this allows services to remain when restarting!)
Now when a thread wants to receive a message from the *same* service, it makes sure the message is signed (or even encrypted) with the service key.
Sending a message to *another* service is just a question of encrypting it with the thread's key and finding the right destination.
To have a thread advertise a service, we can just sign the address with the service's key.
Now we can map human-understandable service names to service keys using web of trust!
So any service you start up yourself is signed using your own key.
(TODO: figure out how to ensure your trust choices get sent to any service you use that uses another service that uses... etc)
Probably we also want a kind of "personal service", e.g. personal messages, which is always trusted (if you trust that the user is who they say they are.)

## Implementing inter-process communication
Discovery works as follows: once a process starts, it notifies others using the multicast address 237.59.133.44 on port 4547. (DO-TIP: figure out the reason behind these numbers!)
The messages have the format (0x01: byte) (port: u16) (length: int) (identifier: utf8), where port is a TCP port that accepts messages for threads, length is the length of the identifier, and identifier is a human-readable, unique-as-possible, name for the process.
Processes can resend this notificiation as much as they want, but please be polite and don't flood the network.

Additionally, a service can request that the process advertises it, in which case the process will send out a the service's certificate (or fingerprint), and a signed message stating at which address the service can be reached.

Messages are sent via TCP, over (TODO WHICH) some encryption/signing algorithm. Each process can listen for incoming TCP connections. 
Which encryption method?
* SSL/TLS: client-server based, apparently requires certificates, rustls requires hostname, not TCP connection
* PGP: better fit with messages to a thread, is one-directional

